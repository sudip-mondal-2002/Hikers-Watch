package com.sudip.hikerswatch;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    LocationManager locationManager;
    LocationListener locationListener;
    ListView listView;
    ArrayList<String> listItems;
    ArrayAdapter listAdapter;

    public class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    private double truncate(double value, int decimal) {
        value = value * Math.pow(10, decimal);
        value = Math.floor(value);
        value = value / Math.pow(10, decimal);
        return value;
    }

    private void updateListView(Location location) {
        String latitude = "Latitude : " + String.valueOf(truncate(location.getLatitude(), 2));
        String longitude = "Longitude : " + String.valueOf(truncate(location.getLongitude(), 2));
        String accuracy = "Accuracy : " + String.valueOf(truncate(location.getAccuracy(), 2));
        String address = "";
        String weather = "Weather\n";
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addressList != null && addressList.size() > 0) {
                String subLocality = addressList.get(0).getSubLocality();
                String locality = addressList.get(0).getLocality();
                String country = addressList.get(0).getCountryName();
                if (subLocality != null) {
                    address += subLocality + "\n";
                }
                if (locality != null) {
                    address += locality + "\n";
                    String result = null;
                    try {
                        DownloadTask downloadTask = new DownloadTask();
                        result = downloadTask.execute("https://api.openweathermap.org/data/2.5/weather?q=" + locality + "&appid=07c153a8414dc274c75e3c33451c85c5").get();
                        JSONObject jsonObject = new JSONObject(result);
                        JSONArray weatherArr = jsonObject.getJSONArray("weather");
                        JSONObject weatherInfo = weatherArr.getJSONObject(0);
                        String main = weatherInfo.getString("main");
                        String description = weatherInfo.getString("description");
                        weather += main + " : " + description + "\n";
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (country != null) {
                        address += country;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        listItems = new ArrayList<String>(Arrays.asList(latitude, longitude, accuracy, address, weather));
        listAdapter = new ArrayAdapter(getApplicationContext(), R.layout.list_custom, listItems);
        listView.setAdapter(listAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                updateListView(lastKnownLocation);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                updateListView(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {

            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {

            }
        };

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            updateListView(lastKnownLocation);
        }
    }
}